from pathlib import Path
import os
from glob import glob
from tqdm import tqdm
from multiprocessing import Pool
import subprocess
from functools import partial
import warnings
import sys
from pdbfixer import PDBFixer
from openmm.app.pdbfile import PDBFile

warnings.filterwarnings("ignore")
BIN_LIB = str(Path(Path(__file__).parent.parent, "bin"))
ADFRSUITE = f"{BIN_LIB}/ADFRsuit/ADFRsuite-1.1dev/bin"
if ADFRSUITE not in sys.path:
    sys.path.append(ADFRSUITE)
PREPARE_RECEPTOR = str(Path(BIN_LIB, "ADFRsuit/ADFRsuite-1.1dev/bin/prepare_receptor"))


def fix_pdb(raw_pdb_path: str, out_pdb_path: str, pH: float):
    fixer = PDBFixer(filename=raw_pdb_path)
    fixer.removeHeterogens(True)
    fixer.findMissingResidues()
    fixer.findNonstandardResidues()
    fixer.findMissingAtoms()
    fixer.addMissingAtoms()
    PDBFile.writeFile(
        fixer.topology, fixer.positions, open(out_pdb_path, "w"), keepIds=True
    )

def run_prepare_receptor(pdb_filepath, output_dir, pH):
    try:
        fixed_pdb = f"{output_dir}/{Path(pdb_filepath).stem}_fixed.pdb"
        fix_pdb(pdb_filepath, fixed_pdb, pH)
        outfile = f"{output_dir}/{Path(pdb_filepath).stem}.pdbqt"
        if not os.path.exists(outfile):
            subprocess.run(
                [PREPARE_RECEPTOR, "-r", fixed_pdb, "-o", outfile, "-A", "hydrogens"],
                stdout=subprocess.PIPE,
            )
    except Exception as e:
        print(pdb_filepath)
        print(e)
        raise e


def main():
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument("-p", "--pdb_dir", type=str, default=None)
    parser.add_argument("-o", "--output_dir", type=str, default=None)
    args = parser.parse_args()
    pdb_files = glob(f"{args.pdb_dir}/*.pdb")
    Path(args.output_dir).mkdir(parents=True, exist_ok=True)
    n_workers = os.cpu_count()
    pfunc = partial(run_prepare_receptor, output_dir=args.output_dir)
    with Pool(n_workers) as p:
        list(tqdm(p.imap(pfunc, pdb_files), total=len(pdb_files)))


if __name__ == "__main__":
    main()
